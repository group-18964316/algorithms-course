public class WeightedQuickUnion {
    private int[] id;
    private int[] sz;

    public WeightedQuickUnion(int N)
    {
        id = new int[N];
        // Set id of each object to itself (N array accesses)
        for (int i = 0; i < N; i++) id[i] = i;
    }

    private int root(int i)
    {
        // Chase parent pointers until reach root
        while (i != id[i]) i = id[i];
        return i;
    }

    // Check if p and q have the same root (depth of p and q array accesses)
    public boolean connected(int p, int q)
    {
        return root(p) == root(q);
    }

    // Change root of p to point to root of q (depth of p and q array accesses)
    public void union(int p, int q)
    {
        int i = root(p);
        int j = root(q);
        id[i] = j;
        // Takes constant time, given roots
        // Depth of any node x is at most base 2 logarithm (lg means logarithm base 2)
        if (i == j) return;
        if (sz[i] < sz[j]) { id[i] = j; sz[j] += sz[i]; }
        else               { id[j] = i; sz[i] += sz[j]; }
    }
}
